package shema3.element;

import schema2.elements.Power;
import shema3.repeator.AbstractRepeater;
import shema3.repeator.Repeater;

public class And extends AbstractElement {

    public And(Class<Repeater> repeater) {
        super(repeater);
    }

    protected void switchAEvent() {
        Power power = this.repeaterA.getResultState();
        this.repeaterB.powerOn(power);
        this.resultState = this.repeaterB.getResultState();
    }

    protected void switchBEvent() {
        this.resultState = this.repeaterB.getResultState();
    }

//    public void calculateResult() {
//        this.resultState = repeaterB.getResultState();
//    }

    @Override
    public String toString() {
        return "\nAnd{" +
                "resultState=" + resultState +
                '}' +
                "\nRepeaterA : " + repeaterA +
                "\n\nRepeaterB : " + repeaterB;
    }
}
