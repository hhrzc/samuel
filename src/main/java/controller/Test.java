package controller;

import schema2.elements.AbstractElement;
import schema2.elements.LogicalInterface;
import schema2.elements.Power;
import schema2.implementation.And;
import schema2.implementation.Or;
import schema2.repitor.Repeater;

import static schema2.elements.Power.OFF;
import static schema2.elements.Power.ON;

public class Test {
    public static void main(String[] args) {
//        LogicalInterface and = new AbstractElement() {
//            public void calculateResultState() {
//                resultState = repeaterA.getResultState().powerIsOn() &&
//                        repeaterB.getResultState().powerIsOn() ?
//                        ON : OFF;
//            }
//        };
//        and.powerOn(ON);
//        and.switchA(ON);
//        and.switchB(ON);
//        System.out.println(and.getResultState());
//        andTest(and);

        LogicalInterface and = new Or(ON);
        System.out.println(and.getResultState());
        and.switchA(ON);
        and.switchA(OFF);
        System.out.println(and.getResultState());

        and.switchB(ON);
        System.out.println(and.getResultState());


//        LogicalInterface or = new Or();
//        or.switchB(ON);
//        System.out.println(or.getResultState());

    }
    private static void andTest(LogicalInterface and) {
        for (int i = 3; i > 0; i--) {
            String s1 = Integer.toBinaryString(i);
            String s = s1.length() < 2 ? "0" + s1
                    : s1;

            and.switchA(getStateByString(s.split("")[0]));
            and.switchB(getStateByString(s.split("")[1]));

            System.out.println(s);
            System.out.println(and.getResultState());
        }
    }


    private static Power getStateByString(String s){
        int i = Integer.parseInt(s);
        return i == 1 ? ON : i == 0 ? OFF : null;
    }
}
