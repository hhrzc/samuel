package schema2.repitor;

import schema2.elements.Power;

public interface BaseElement {
    void powerOn(Power power);
    void calculateResultState();
    Power getResultState();
}
