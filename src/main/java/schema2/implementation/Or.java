package schema2.implementation;

import schema2.elements.AbstractElement;
import schema2.elements.Power;
import schema2.repitor.Repeater;
import schema2.repitor.RepeaterInterface;
import schema2.repitor.Revert;

public class Or extends AbstractElement<Revert> {

    public Or()
    {
        this(Power.ON);
    }

    public Or(Power on) {
        super(Revert.class, on);
        calculateResultState();
    }

    public void calculateResultState() {
        resultState = repeaterA.getResultState().powerIsOn() ||
        repeaterB.getResultState().powerIsOn() ? Power.ON : Power.OFF;
    }
}
