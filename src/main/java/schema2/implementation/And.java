package schema2.implementation;

import schema2.elements.AbstractElement;
import schema2.elements.Power;
import schema2.repitor.Repeater;
import schema2.repitor.RepeaterInterface;

import static schema2.elements.Power.OFF;
import static schema2.elements.Power.ON;

public class And extends AbstractElement<Repeater> {

    public And(Power power){
        super(Repeater.class, power);
    }

    public And() {
        this(OFF);
    }

    public void calculateResultState() {
        resultState = repeaterA.getResultState().powerIsOn() &&
                repeaterB.getResultState().powerIsOn() ?
                ON : OFF;
    }
}
